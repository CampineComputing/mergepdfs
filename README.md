# Merge multiple PDFs while starting each PDF at an odd page

`mergepdfs` is a simple command-line program to merge multiple input PDF files into
a single PDF file. It ensures that each of the merged PDF files starts at an
odd page in the merged output file. As a result, you can print the output PDF
with the setting "double-sided" without printing the first page of an input
PDF file on the same sheet at the last page of the previous input PDF file.

`mergepdfs` is free software; `mergepdfs` is released under the AGPL 3.0.

## Motivation

When I do research on some topic, I start by looking for papers and books on
that topic on the internet. In a short while I collect a lot of PDF files
related to my research. I like to have also a paper copy of all the relevant
papers.

I prefer reading from paper. I like to scribble notes on them. And
when writing about my research, I like to represent my argument with stacks of
physical papers. 

However, printing all these PDF files on my small home
printer is a bit of a hassle. Instead, I like to use an online print service!
I upload a PDF file, and they print it and send it to me. Of course, uploading
all the PDF files I want printed separately is a hassle. Instead I merge all
the PDF files I want to print into a single PDF file and have that printed.

Because I have my papers printed double-sided, it is possible that pages of
two papers in the merged PDF file end up on the same sheet. To prevent that
from happening, and keep all papers separate, each paper in the merged PDF
file should start at an odd page. For this use case, I wrote `mergedpdfs`.

## Installation

`mergepdfs` is a Ruby program. You can install it by entering the following in
your terminal:

```{bash}
gem install mergepdfs
```

## Usage

`mergepdfs` is easy to use. Specify the filename of the result of the merging and the input PDF files to merge:

```
mergepdfs --output FILENAME [OPTIONS] file1.pdf file2.pdf ... fileN.pdf

Required:
-o, --output FILENAME            The name of the output PDF file. If no filename is specified,
                                 'output.pdf' is used by default.

Optional:
-c, --continue-on-error          When an error occurs merging a PDF file, mergepdfs tries
to continue merging the other PDF files. Default is false.
-d, --diagnostics                Show detailed error messages. Default is false.

Common:
-v, --version                    Show the version.
-h, --help                       Show this help message.
```

**Example.** Merge all PDF files in this directory to `syllabus.pdf`

```{bash}
mergepdfs -o syllabus.pdf *.pdf
```

or

```{bash}
mergepdfs --output syllabus.pdf article1.pdf another_report.pdf download\(1\).pdf
```

