require_relative 'lib/mergepdfs'

Gem::Specification.new do |spec|
  spec.name          = "mergepdfs"
  spec.license       = "GPL-3.0-or-later"
  spec.version       = MergePDFs::VERSION
  spec.date          = MergePDFs::RELEASE_DATE
  spec.authors       = ["Huub de Beer"]
  spec.email         = ["huub@campinecomputing.eu"]
  spec.summary       = %q{mergepdfs combines multiple PDF files into one output PDF file, each starting at an odd page.}
  spec.description   = %q{mergepdfs combines multiple PDF files into one output PDF file. Empty pages are added to ensure that each merged PDF file starts at an odd page number in the output file. When printed double-sided, pages of the merged PDF files are never printed on the same sheet.}
  spec.homepage      = "https://gitlab.com/CampineComputing/mergepdfs"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")
  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/CampineComputing/mergepdfs"
  spec.files         = ["lib/mergepdfs.rb"]
  spec.executables   = ["mergepdfs"]
  spec.require_paths = ["lib"]
  spec.add_runtime_dependency "hexapdf", "~>0.11.5"
end
